package com.prabowo.servergps;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.control.Button;

public class FXMLController implements Initializable {
    
    @FXML
    private Label label;
    @FXML
    private Button openServer;
    
    private SocketManage server;
    
    @FXML
    private void openServerAction(ActionEvent event) throws InterruptedException{
        
        
        new Thread(server).start();
        
        openServer.setDisable(true);
    }
    
    @FXML
    private void closeServerAction(){
        openServer.getScene().getWindow().hide();
    }
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        server = new SocketManage(54581);
    }    
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.prabowo.servergps;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.Socket;

public class SocketClientManage implements Runnable{
        
    private final Socket client;

    SocketClientManage(Socket client) {
        this.client = client;
    }

    @Override
    public void run() {
        try{
            BufferedReader in = new BufferedReader(new InputStreamReader(this.client.getInputStream()));
            String nextline;
            while ((nextline = in.readLine())!=null) {
               System.out.println(nextline);
            } 
        }catch(IOException e){
            e.printStackTrace();
        }
    }

}
